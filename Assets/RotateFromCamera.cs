﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateFromCamera : MonoBehaviour {

    public enum Direction { Up,Down,Left,Right,Center}

    public float m_rotationSpeed = 90;
    public Vector2 m_direction;

    public void SetHorizontal(float speed)
    {
        m_direction.x = speed;
    }
    public void SetVertical(float speed)
    {

        m_direction.y = speed;
    }
 

	void Update ()
    {
        transform.Rotate(Camera.main.transform.up, m_rotationSpeed * Time.deltaTime * -m_direction.x, Space.World);
        transform.Rotate(Camera.main.transform.right, m_rotationSpeed * Time.deltaTime* m_direction.y, Space.World);
    }
}
