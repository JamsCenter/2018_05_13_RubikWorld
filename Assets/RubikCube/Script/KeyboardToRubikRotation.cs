﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyboardToRubikRotation : MonoBehaviour {

    public RubikCube m_affectedRubikCube;

	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        bool foundOne = false;
        RubikCubeFace faceToTurn = GetFaceToTurn(out foundOne);

        if(foundOne)
         m_affectedRubikCube.LocalRotate(faceToTurn, GetClockDirection());

        if (Input.GetKeyDown(KeyCode.End))
            m_affectedRubikCube.ResetInitialState();
    }

    private static bool GetClockDirection()
    {
        return !( Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift));
    }

    private static RubikCubeFace GetFaceToTurn(out bool foundOne)
    {
        foundOne = false;
        RubikCubeFace faceToTurn = RubikCubeFace.Middle;
        if (Input.GetKeyDown(KeyCode.B))
        {
            faceToTurn = RubikCubeFace.Back;
            foundOne = true;
        }
        else if (Input.GetKeyDown(KeyCode.D))
        {
            faceToTurn = RubikCubeFace.Down;
            foundOne = true;
        }
        else if (Input.GetKeyDown(KeyCode.E))
        {
            faceToTurn = RubikCubeFace.Equator;
            foundOne = true;
        }
        else if (Input.GetKeyDown(KeyCode.F))
        {
            faceToTurn = RubikCubeFace.Face;
            foundOne = true;
        }
        else if (Input.GetKeyDown(KeyCode.L))
        {
            faceToTurn = RubikCubeFace.Left;
            foundOne = true;
        }
        else if (Input.GetKeyDown(KeyCode.M))
        {
            faceToTurn = RubikCubeFace.Middle;
            foundOne = true;
        }
        else if (Input.GetKeyDown(KeyCode.R))
        {
            faceToTurn = RubikCubeFace.Right;
            foundOne = true;
        }
        else if (Input.GetKeyDown(KeyCode.S))
        {
            faceToTurn = RubikCubeFace.Standing;
            foundOne = true;
        }
        else if (Input.GetKeyDown(KeyCode.U))
        {
            faceToTurn = RubikCubeFace.Up;
            foundOne = true;
        }

        return faceToTurn;
    }
}
