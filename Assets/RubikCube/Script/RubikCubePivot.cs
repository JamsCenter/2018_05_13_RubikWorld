﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RubikCubePivot : MonoBehaviour {

    public Transform m_root;
    public RubikCubeFace m_face;
    public bool m_isAtCenter;

    private void Reset()
    {
        m_root = this.transform;
    }
}
