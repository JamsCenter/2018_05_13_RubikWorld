﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System.Linq;

//Info https://ruwix.com/the-rubiks-cube/fingertricks/

public interface IRubikCube {



}
/// https://ruwix.com/the-rubiks-cube/notation/
public enum RotationTypeShort
{
    L, Lp,
    R, Rp,
    U, Up,
    D, Dp,
    F, Fp,
    B, Bp,
    M, Mp,
    E, Ep,
    S, Sp,
    X, Xp,
    Y, Yp,
    Z, Zp
}
public enum RotationTypeLong
{
    Left, LeftCounter,
    Right, RightCounter,
    Up, UpCounter,
    Down, DownCounter,
    Face, FaceCounter,
    Back, BackCounter,
    Middle, MiddleCounter,
    Equator, EquatorCounter,
    Standing, StandingCounter,
    X, XCounter,
    Y, YCounter,
    Z, ZCounter
}
public enum RubikCubeFace
{
    Left, 
    Right, 
    Up, 
    Down, 
    Face, 
    Back, 
    Middle,
    Equator,
    Standing
   
}
public enum RubikPieceType :int {

    X2_Y2_Z2 = -1,
    X1_Y1_Z1 = 0,
    X1_Y1_Z2 = 1,
    X1_Y1_Z3 = 2,
    X1_Y2_Z1 = 3,
    X1_Y2_Z2 = 4,
    X1_Y2_Z3 = 5,
    X1_Y3_Z1 = 6,
    X1_Y3_Z2 = 7,
    X1_Y3_Z3 = 8,

    X2_Y1_Z1 = 9,
    X2_Y1_Z2 = 10,
    X2_Y1_Z3 = 11,
    X2_Y2_Z1 = 12,
    X2_Y2_Z3 = 13,
    X2_Y3_Z1 = 14,
    X2_Y3_Z2 = 15,
    X2_Y3_Z3 = 16,

    X3_Y1_Z1 = 17,
    X3_Y1_Z2 = 18,
    X3_Y1_Z3 = 19,
    X3_Y2_Z1 = 20,
    X3_Y2_Z2 = 21,
    X3_Y2_Z3 = 22,
    X3_Y3_Z1 = 23,
    X3_Y3_Z2 = 24,
    X3_Y3_Z3 = 25,
}



public class RubikCube : MonoBehaviour {

    [Header("Params")]
    public Transform m_root;

    [Header("Pivot")]
    public Transform m_left;
    public Transform m_right;
    public Transform m_up;
    public Transform m_down;
    public Transform m_face;
    public Transform m_back;
    public Transform m_middle;
    public Transform m_equator;
    public Transform m_standing;
    public RubikCubePivot[] m_pivots;

    public RubikCubeSpot[] m_piecesSpots;
    public Dictionary<RubikCubeFace, List<RubikCubeSpot>> m_faces = new Dictionary<RubikCubeFace, List<RubikCubeSpot>>();


    public RubikPiece[] m_pieces;

    public RubikCubeRotateMotor m_rotationMotor;

    void Awake() {

        foreach (RubikCubeSpot spot in m_piecesSpots)
        {
            foreach (RubikCubeFace face in spot.m_faces)
            {
                AddSpotToToRegister(face, spot);
            }

        }
        SaveInitialState();

    }



    public void DebugDisplayFace(RubikCubeFace face, float time, Color color)
    {
        List<RubikCubeSpot> spots = GetSpots(face);
        foreach (RubikCubeSpot spot in spots)
        {
            DebugUtility.DrawCross(spot.m_root, 0.2f, color, time);

        }
    }

    private List<RubikCubeSpot> GetSpots(RubikCubeFace face)
    {
        return m_faces[face];
    }

    private void AddSpotToToRegister(RubikCubeFace face, RubikCubeSpot spot)
    {
        if (!m_faces.ContainsKey(face))
            m_faces.Add(face, new List<RubikCubeSpot>());
        m_faces[face].Add(spot);
    }

    void Update() {

    }

    internal Transform GetPivot(RubikCubeFace faceToTurn)
    {
        switch (faceToTurn)
        {
            case RubikCubeFace.Left: return m_left;
            case RubikCubeFace.Right: return m_right;
            case RubikCubeFace.Up: return m_up;
            case RubikCubeFace.Down: return m_down;
            case RubikCubeFace.Face: return m_face;
            case RubikCubeFace.Back: return m_back;
            case RubikCubeFace.Middle: return m_middle;
            case RubikCubeFace.Equator: return m_equator;
            case RubikCubeFace.Standing: return m_standing;
        }
        return null;
    }

    internal RubikPiece[] GetPieces(RubikCubeFace face)
    {
        List<RubikCubeSpot> spots = GetSpots(face);
        List<RubikPiece> pieces = new List<RubikPiece>();
        foreach (RubikCubeSpot spot in spots)
        {
            RubikPiece piece = GetClosestPieceOf(spot.m_root);
            pieces.Add(piece);
        }
        return pieces.ToArray();
    }

    private RubikPiece GetClosestPieceOf(Transform m_root)
    {
        RubikPiece closest = null;
        float smallestdistance = float.MaxValue;
        foreach (RubikPiece piece in m_pieces)
        {
            float dist = Vector3.Distance(m_root.position, piece.m_root.position);
            if (dist < smallestdistance)
            {
                smallestdistance = dist;
                closest = piece;
            }

        }
        return closest;
    }

    internal void LocalRotate(RubikCubeFace faceToTurn, bool clockWise)
    {
        m_rotationMotor.LocalRotate(faceToTurn, clockWise);
    }

    #region ROTATION LISTENER
    [Header("Events")]
    public RotationEvent m_onStartRotating;
    public RotationEvent m_onRotated;

    [System.Serializable]
    public class RotationEvent : UnityEvent<LocalRotationRequest> {

    }
    [System.Serializable]
    public class LocalRotationRequest
    {
        public RubikCubeFace m_faceToRotate;
        public bool m_clockWise;

        public LocalRotationRequest(RubikCubeFace face, bool clockWise)
        {
            this.m_faceToRotate = face;
            this.m_clockWise = clockWise;
        }
    }
    public void NotifyStartRotation(RubikCubeFace face, bool clockWise)
    {
        NotifyStartRotation(new LocalRotationRequest(face, clockWise));
    }
    public void NotifyEndRotation(RubikCubeFace face, bool clockWise)
    {
        NotifyStartRotation(new LocalRotationRequest(face, clockWise));
    }
    public void NotifyStartRotation(LocalRotationRequest req)
    {
        m_onStartRotating.Invoke(req);
    }
    public void NotifyEndRotation(LocalRotationRequest req)
    {
        m_onRotated.Invoke(req);
    }

    #endregion

    #region Solution LISTENER

    public RubikCubeResolvedState m_cubeResolvedState;
    public UnityEvent m_onCubeResolved;
    public bool IsCubeResolved()
    {
        float pct;
        return IsCubeResolved(out pct);
    }
    public bool IsCubeResolved(out float pourcent)
    {
        float pct = 0;
        bool resolved = m_cubeResolvedState.IsResolved(out pct);
        pourcent = pct;
        return resolved;
    }

    public void NotifyCubeAsResolved() {
        m_onCubeResolved.Invoke();
    }

    #endregion
    //internal static void AddLocalRotationRequest(RotationTypeLong face)
    //{
    //    throw new NotImplementedException();
    //}

    //internal static void AddRotationRequest(RotationTypeLong face, Transform m_viewOrientation)
    //{
    //    throw new NotImplementedException();
    //}

    public void LocalRotateWithAcronym(string requestAcryonym) {
        RotationTypeShort acronym = ConvertStringToShortAcronym(requestAcryonym);
        LocalRotate(acronym);
    }



    public void LocalRotate(RotationTypeShort instruction) {
        RubikCubeFace faceToRotate;
        bool clockwise;
        ConvertAcronymToFaceRotation(instruction, out faceToRotate, out clockwise);
        LocalRotate(faceToRotate, clockwise);

    }

    public static  void ConvertAcronymToFaceRotation(RotationTypeShort instruction, out RubikCubeFace faceToRotate, out bool clockwise)
    {
        switch (instruction)
        {
            case RotationTypeShort.L: clockwise = true; faceToRotate = RubikCubeFace.Left;
                break;
            case RotationTypeShort.Lp:
                clockwise = false; faceToRotate = RubikCubeFace.Left;
                break;
            case RotationTypeShort.R:
                clockwise = true; faceToRotate = RubikCubeFace.Right;
                break;
            case RotationTypeShort.Rp:
                clockwise = false; faceToRotate = RubikCubeFace.Right;
                break;
            case RotationTypeShort.U:
                clockwise = true; faceToRotate = RubikCubeFace.Up;
                break;
            case RotationTypeShort.Up:
                clockwise = false; faceToRotate = RubikCubeFace.Up;
                break;
            case RotationTypeShort.D:
                clockwise = true; faceToRotate = RubikCubeFace.Down;
                break;
            case RotationTypeShort.Dp:
                clockwise = false; faceToRotate = RubikCubeFace.Down;
                break;
            case RotationTypeShort.F:
                clockwise = true; faceToRotate = RubikCubeFace.Face;
                break;
            case RotationTypeShort.Fp:
                clockwise = false; faceToRotate = RubikCubeFace.Face;
                break;
            case RotationTypeShort.B:
                clockwise = true; faceToRotate = RubikCubeFace.Back;
                break;
            case RotationTypeShort.Bp:
                clockwise = false; faceToRotate = RubikCubeFace.Back;
                break;
            case RotationTypeShort.M:
                clockwise = true; faceToRotate = RubikCubeFace.Middle;
                break;
            case RotationTypeShort.Mp:
                clockwise = false; faceToRotate = RubikCubeFace.Middle;
                break;
            case RotationTypeShort.E:
                clockwise = true; faceToRotate = RubikCubeFace.Equator;
                break;
            case RotationTypeShort.Ep:
                clockwise = false; faceToRotate = RubikCubeFace.Equator;
                break;
            case RotationTypeShort.S:
                clockwise = true; faceToRotate = RubikCubeFace.Standing;
                break;
            case RotationTypeShort.Sp:
                clockwise = false; faceToRotate = RubikCubeFace.Standing;
                break;
            default:
                clockwise = true; faceToRotate = RubikCubeFace.Middle;
                break;
        }
    }

    public static string GetAcronymShortString(RubikCubeFace faceToRotate, bool clockWise)
    {
        RotationTypeShort rotType = ConvertRotationToAcronymShort(faceToRotate, clockWise);
        switch (rotType)
        {
            case RotationTypeShort.L: return "L";
            case RotationTypeShort.Lp: return "L'";
            case RotationTypeShort.R: return "R";
            case RotationTypeShort.Rp: return "R'";
            case RotationTypeShort.U: return "U";
            case RotationTypeShort.Up: return "U'";
            case RotationTypeShort.D: return "D";
            case RotationTypeShort.Dp: return "D'";
            case RotationTypeShort.F: return "F";
            case RotationTypeShort.Fp: return "F'";
            case RotationTypeShort.B: return "B";
            case RotationTypeShort.Bp: return "B'";
            case RotationTypeShort.M: return "M";
            case RotationTypeShort.Mp: return "M'";
            case RotationTypeShort.E: return "E";
            case RotationTypeShort.Ep: return "E'";
            case RotationTypeShort.S: return "S";
            case RotationTypeShort.Sp: return "S'";
            case RotationTypeShort.X: return "X";
            case RotationTypeShort.Xp: return "X'";
            case RotationTypeShort.Y: return "Y";
            case RotationTypeShort.Yp: return "Y'";
            case RotationTypeShort.Z: return "Z";
            case RotationTypeShort.Zp: return "Z'";
            default: return "?";
        }


    }

    public static RotationTypeShort ConvertRotationToAcronymShort(RubikCubeFace faceToRotate, bool clockWise)
    {
        switch (faceToRotate)
        {
            case RubikCubeFace.Left: return clockWise ? RotationTypeShort.L : RotationTypeShort.Lp;
            case RubikCubeFace.Right: return clockWise ? RotationTypeShort.R : RotationTypeShort.Rp;
            case RubikCubeFace.Up: return clockWise ? RotationTypeShort.U : RotationTypeShort.Up;
            case RubikCubeFace.Down: return clockWise ? RotationTypeShort.D : RotationTypeShort.Dp;
            case RubikCubeFace.Face: return clockWise ? RotationTypeShort.F : RotationTypeShort.Fp;
            case RubikCubeFace.Back: return clockWise ? RotationTypeShort.B : RotationTypeShort.Bp;
            case RubikCubeFace.Middle: return clockWise ? RotationTypeShort.M : RotationTypeShort.Mp;
            case RubikCubeFace.Equator: return clockWise ? RotationTypeShort.E : RotationTypeShort.Ep;
            case RubikCubeFace.Standing: return clockWise ? RotationTypeShort.S : RotationTypeShort.Sp;
            default:
                return clockWise ? RotationTypeShort.E : RotationTypeShort.Ep;
        }
    }

    public static RotationTypeShort ConvertStringToShortAcronym(string requestAcryonym)
    {
        requestAcryonym = requestAcryonym.ToLower();
        switch (requestAcryonym)
        {
            case "b": return RotationTypeShort.B;
            case "b'": return RotationTypeShort.Bp;
            case "d": return RotationTypeShort.D;
            case "d'": return RotationTypeShort.Dp;
            case "e": return RotationTypeShort.E;
            case "e'": return RotationTypeShort.Ep;
            case "f": return RotationTypeShort.F;
            case "f'": return RotationTypeShort.Fp;
            case "l": return RotationTypeShort.L;
            case "l'": return RotationTypeShort.Lp;
            case "m": return RotationTypeShort.M;
            case "m'": return RotationTypeShort.Mp;

            case "r": return RotationTypeShort.R;
            case "r'": return RotationTypeShort.Rp;

            case "s": return RotationTypeShort.S;
            case "s'": return RotationTypeShort.Sp;

            case "u": return RotationTypeShort.U;
            case "u'": return RotationTypeShort.Up;
            case "x": return RotationTypeShort.X;
            case "x'": return RotationTypeShort.Xp;

            case "y": return RotationTypeShort.Y;
            case "y'": return RotationTypeShort.Yp;

            case "z": return RotationTypeShort.Z;
            case "z'": return RotationTypeShort.Zp;

            default: return RotationTypeShort.S;

        }
    }


    public void SaveInitialState() {
        List<PieceInitialState> state = new List<PieceInitialState>();
        foreach (RubikPiece piece in m_pieces)
        {
            state.Add(new PieceInitialState(piece, piece.transform.localRotation, piece.transform.localPosition));
        }
        m_initialState = state.ToArray();

    }
    public void ResetInitialState() {

        foreach (PieceInitialState state in m_initialState)
        {
            state.m_linkedPiece.transform.localPosition = state.m_initialPosition;
            state.m_linkedPiece.transform.localRotation = state.m_initialRotation;
        }

    }

    internal PieceInitialState[] GetInitialSpots()
    {
        return m_initialState;
    }

    internal RubikPiece[] GetPieces()
    {
        return m_pieces;
    }

    private PieceInitialState[] m_initialState;

    [Serializable]
    public class PieceInitialState
    {
        public RubikPiece m_linkedPiece;
        public Quaternion m_initialRotation;
        public Vector3 m_initialPosition;

        public PieceInitialState(RubikPiece piece, Quaternion localRotation, Vector3 localPosition)
        {
            this.m_linkedPiece = piece;
            this.m_initialRotation = localRotation;
            this.m_initialPosition = localPosition;
        }
    }


    #region SHUFFLE
    public void ShuffleFace() {
        ShuffleFace(20);
    }
    public void ShuffleFace(int timeToRotate)
    {
        for (int i = 0; i < timeToRotate; i++)
        {
            LocalRotate(GetRandomFace(), GetRandomDirection());
        }
    }

    public static RubikCubeFace GetRandomFace()
    {
        int face = UnityEngine.Random.Range(0, 9);
        switch (face)
        {
            case 0: return RubikCubeFace.Back;
            case 1: return RubikCubeFace.Down;
            case 2: return RubikCubeFace.Equator;
            case 3: return RubikCubeFace.Face;
            case 4: return RubikCubeFace.Left;
            case 5: return RubikCubeFace.Middle;
            case 6: return RubikCubeFace.Right;
            case 7: return RubikCubeFace.Standing;
            case 8: return RubikCubeFace.Up;
            default:
                return RubikCubeFace.Middle;
        }
    }

    private bool GetRandomDirection()
    {
        return UnityEngine.Random.Range(0, 2) == 0;
    }
    #endregion

}
