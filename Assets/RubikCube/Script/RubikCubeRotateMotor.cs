﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class RubikCubeRotateMotor : MonoBehaviour {

    public abstract bool IsRotating();
    public abstract void LocalRotate(RubikCubeFace faceToRotate, bool clockwise);
    public abstract void LocalRotate(RotationTypeShort faceToRotate);

    [Header("Events")]
    public RubikCube.RotationEvent m_onStartRotating;
    public RubikCube.RotationEvent m_onRotated;

    protected virtual void NotifyStartRotation(RubikCubeFace face, bool clockWise)
    {
        m_onStartRotating.Invoke(new RubikCube.LocalRotationRequest(face, clockWise));
    }
    protected virtual void NotifyEndRotation(RubikCubeFace face, bool clockWise)
    {
        m_onRotated.Invoke(new RubikCube.LocalRotationRequest(face, clockWise));
    }
}
